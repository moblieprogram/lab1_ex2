import 'dart:io';

void main(List<String> args) {
  String? input = stdin.readLineSync()!;
  List<String> words = input.toLowerCase().split(' ');
  var text = {};

  for (var word in words) {
    if (!text.containsKey(word)) {
      text[word] = 1;
    } else {
      text[word] += 1;
    }
  }

  print(text);
}
